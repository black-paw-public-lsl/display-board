# Change Log

## Version 3.0.1

* Fix `display-notecard-example.lsl`.
* Add extra documentation files.

## Version 3.0.0

* Move constants (`#define`s) to their own file so they can be shared.
* Rework constants so they're not so strange.
* Simplify the controller a bit.

## Version 2.3.0

* Add another font.
* Add control message.

## Version 2.2.0

* Add extra fonts.
* Add control messages such as font and color selection, etc.

## Version 2.1.0

* Deep clean up of code.
