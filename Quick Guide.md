# Display Board Developer Kit

Black Paw Workshop hopes you find the display board straightforward to program and works well.

## ! ! Notice ! !

Please understand this is a developer's kit.
You need to already have experience scripting.

Black Paw Workshop cannot give lesson in scripting or building objects.

## Quick Guide

* Edit the display board.
* Remove the `Display Board Demo` script.
* **Leave the `Display Board Controller` script in the display board.**
  This is what makes the board work.
* Add your own script.
* If you accidentally delete the `Display Board Controller` script,
  the kit contains copies of the script.

All documentation, full source code, examples, etc. can be found at:

https://gitlab.com/black-paw-public-lsl/display-board

## Support

For support, suggestions and feedback you may:

* Join the Black Paw Workshop in-world group.
* Send SecondLife resident BlackPawWorkshop a notecard.
* Join the Black Paw Workshop Discord server at https://discord.gg/vreVAwUc .

Black Paw Workshop will respond as fast and helpful as she can.

Copyright 2021, BlackPawWorkshop
