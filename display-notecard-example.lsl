////////////////////////////////////////////////////////////////////////
//
// Display Notecard Example Script
// by Black Paw Workshop
//
// DESCRIPTION
// This script waits for a notecard to be dropped into the display board,
// tries to read the contents and display them.
//
// The master copy of this script is hosted at:
// https://gitlab.com/black-paw-public-lsl/display-board
//
// This work is covered by a Creative Commons
// Attribution-NonCommercial-ShareAlike 4.0 license.
//
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
////////////////////////////////////////////////////////////////////////

#include "display-board/display-board-demo-constants.lsl"

string  name;
integer line;
key queryHandle;

displayCard()
{
    name = llGetInventoryName(INVENTORY_NOTECARD, 0);

    if (llGetInventoryType(name) == INVENTORY_NOTECARD)
    {
        llSay(0, "Attempting to read and display the notecard named " + name + ".");
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_FONT_BOLD, "", NULL_KEY); // Select plain bold font
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_FULLBRIGHT_ON, "", NULL_KEY); // Full bright
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_CLEAR_DISPLAY, "", NULL_KEY); // Clear display
        line = 0;
        queryHandle = llGetNotecardLine(name, line);
    }
}

showHelp()
{
    llSay(0, "Drop a notecard in. If the notecard can be read, it will be displayed on the display board.");
}

default
{
    changed(integer intChange)
    {
        if (intChange & CHANGED_INVENTORY)
        {   
            displayCard();
        }
    }

    dataserver(key id, string data)
    {
        if (id == queryHandle)
        {
            if (data == EOF)
            {
                llSay(0, "Done reading notecard. Removing it.");
                llRemoveInventory(name);
                return;
            }

            // Display line.
            llMessageLinked(LINK_ROOT, DISPLAY_BOARD_TEXT + line, data, NULL_KEY);

            // Advance to next line.
            line++;
            queryHandle = llGetNotecardLine(name, line);
        }
    }

    state_entry()
    {
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_FONT_BOLD, "", NULL_KEY);
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_FULLBRIGHT_ON, "", NULL_KEY);
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_CLEAR_DISPLAY, "", NULL_KEY);

        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_TEXT + 0, "Notecard Display", NULL_KEY);
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_TEXT + 1, "Drop a notecard in.", NULL_KEY);
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_TEXT + 3, "If the notecard can be read, it will be", NULL_KEY);
        llMessageLinked(LINK_ROOT, DISPLAY_BOARD_TEXT + 4, "displayed.", NULL_KEY);
    }

    touch_start( integer num_detected )
    {
        showHelp();
    }
}
