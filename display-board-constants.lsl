////////////////////////////////////////////////////////////////////////
//
// Display Board Controller Constants
// by Black Paw Workshop
//
// The master copy of this script is hosted at:
// https://gitlab.com/black-paw-public-lsl/display-board
//
// This work is covered by a Creative Commons
// Attribution-NonCommercial-ShareAlike 4.0 license.
//
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
////////////////////////////////////////////////////////////////////////

// Information originating from the controller script to other scripts
#define DISPLAY_BOARD_TO_EXTERNAL_SCRIPT_NUMBER 101000
#define BOOTING_MESSAGE "STARTING"
#define READY_MESSAGE "READY"

// Information originating from others scripts to the controller script.
#define EXTERNAL_SCRIPT_TO_DISPLAY_BOARD_NUMBER 102000

#define OFFSET_TEXT                 0

#define OFFSET_SHOW_TEST_PATTERN 1000
#define OFFSET_QUERY_STATE       1001

#define OFFSET_FULLBRIGHT_ON     1100
#define OFFSET_FULLBRIGHT_OFF    1101
#define OFFSET_SET_COLOR         1200
#define OFFSET_CLEAR_ALL         1300
#define OFFSET_CLEAR_BODY        1301

#define OFFSET_FONT_SET1         3001
#define OFFSET_FONT_SET2         3002
#define OFFSET_FONT_SET3         3003
#define OFFSET_FONT_SET4         3004
#define OFFSET_FONT_SET5         3005
#define OFFSET_FONT_SET6         3006
#define OFFSET_FONT_SET7         3007
#define OFFSET_FONT_SET8         3008
#define OFFSET_FONT_SET9         3009
#define OFFSET_FONT_SET10        3010
