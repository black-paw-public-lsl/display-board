# Display Board Developer Kit

*By Black Paw Workshop*

https://gitlab.com/black-paw-public-lsl/display-board

## Overview

This is a generic ASCII display board controlled via link messages.

**To use this display board, you must understand scripting,
especially sending link messages.
Black Paw Workshop cannot give lessons for scripting or object building.**

The root prim is an ordinary prim.
It may be retextured as desired
-- or replaced if you're adventurous and careful.

## Display Layout

* The display title line has 24 characters.
* The next ten display body lines have 40 characters.

## Display Board Control Script

### Startup

When the display board control script starts,
it sends a link message number 101000 with the text `STARTING`.
It then will take a few seconds to auto-configure itself.

When the board is ready,
it sends a link message number 101000 with the text `READY`.

The state of the control script may be queried by sending a link message with number 101993.
The script with respond with either `STARTING` or `READY`.

### Displaying Text

The control script is line-oriented,
meaning a whole line of text is updated at once.
If you wish to control individual characters,
you will need to examine the control script and
determine how to do this yourself.

The link message number controls which line is altered:

| Link Number | Line                                           |
| :---------- | :--------------------------------------------- |
| 102000      | Set the display title.                         |
| 102001      | Set the first line of the display body.        |
| 102002      | Set the second line of the display body.       |
| ...         | ... etc. ...                                   |
| 102010      | Set the last (tenth) line of the display body. |

In other words,
the number 102000 + n will update the nth body line.

#### Example 1

```lsl
llMessageLinked(LINK_ROOT, 102000, "Announcements", NULL_KEY);
llMessageLinked(LINK_ROOT, 102001, "Today's menu is:", NULL_KEY);
llMessageLinked(LINK_ROOT, 102002, "* Chicken", NULL_KEY);
llMessageLinked(LINK_ROOT, 102003, "* Tuna", NULL_KEY);
llMessageLinked(LINK_ROOT, 102004, "* Catnip salad", NULL_KEY);
```

### Display Test Pattern

A test pattern can be shown by sending a link message.

| Link Number | Action                 |
| :---------- | :--------------------- |
| 103000      | Show the test pattern. |

#### Example 2

```lsl
llMessageLinked(LINK_ROOT, 103000, "", NULL_KEY);
```

### Text Full Bright

Full bright for the text can be enabled or disabled by sending link messages.
This affects future text sent.

| Link Number | Action                      |
| :---------- | :-------------------------- |
| 103100      | Turns text full bright on.  |
| 103101      | Turns text full bright off. |

#### Example 3

```lsl
llMessageLinked(LINK_ROOT, 103100, "", NULL_KEY);
```

### Set Text Color

Future text color can be set by sending a link message.
The `str` parameter of `llMessageLinked` must contain the color vector as a string.
This affects future text sent.

| Link Number | Action               |
| :---------- | :------------------- |
| 103200      | Sets the text color. |

#### Example 4

```lsl
// Change color to Red
llMessageLinked(LINK_ROOT, 103200, "<1,0,0>", NULL_KEY);
llMessageLinked(LINK_ROOT, 102000, "Announcements", NULL_KEY);
// Change color to White
llMessageLinked(LINK_ROOT, 103200, "<1,1,1>", NULL_KEY);
llMessageLinked(LINK_ROOT, 102001, "Today's menu is:", NULL_KEY);
llMessageLinked(LINK_ROOT, 102002, "* Chicken", NULL_KEY);
llMessageLinked(LINK_ROOT, 102003, "* Tuna", NULL_KEY);
llMessageLinked(LINK_ROOT, 102004, "* Catnip salad", NULL_KEY);
```

### Clear Display

| Link Number | Action                         |
| :---------- | :----------------------------- |
| 103300      | Clear the entire display.      |
| 103301      | Clear the body of the display. |

#### Example 5

```lsl
llMessageLinked(LINK_ROOT, 103300, "", NULL_KEY);
```

### Query Controller Status

| Link Number | Action                                     |
| :---------- | :----------------------------------------- |
| 103001      | Ask controller to send its current status. |

The controller will respond in the section *Startup* above,
with either `STARTING` or `READY`.

### Select Font

There are a few fonts that come with the display board.
The font for future text can be selected by sending a link message.

| Link Number | Font               |
| :---------- | :----------------- |
| 105001      | Standard font      |
| 105002      | Bold standard font |
| 105003      | Medieval font      |
| 105004      | Marker font        |
| 105005      | Southwestern font  |
| 105006      | Bold serif font    |
| 105007      | Dripping font      |
| 105008      | Metal font         |
| 105009      | Future font        |

#### Example 6

```lsl
// Change font to marker
llMessageLinked(LINK_ROOT, 105004, "", NULL_KEY);
llMessageLinked(LINK_ROOT, 102000, "Announcements", NULL_KEY);
// Change font to standard bold text
llMessageLinked(LINK_ROOT, 105002, "", NULL_KEY);
llMessageLinked(LINK_ROOT, 102001, "Today's menu is:", NULL_KEY);
llMessageLinked(LINK_ROOT, 102002, "* Chicken", NULL_KEY);
llMessageLinked(LINK_ROOT, 102003, "* Tuna", NULL_KEY);
llMessageLinked(LINK_ROOT, 102004, "* Catnip salad", NULL_KEY);
```

## Script Details

The script uses Firestorm script extensions to
allow for the use of
`#define`, `#include` and `//` comments.

### Script Communication

#### Information To Other Scripts

The link number broadcast while the script is starting can be altered
by changing the line:

```cpp
#define DISPLAY_BOARD_TO_EXTERNAL_SCRIPT_NUMBER 101000
```

#### Other Scripts To This Script

The link message number to update the board can be altered
by changing the line:

```cpp
#define EXTERNAL_SCRIPT_TO_DISPLAY_BOARD_NUMBER 102000
```

All commands to the display board are relative to this number:

| Link Number | Action                            |
| :---------- | :-------------------------------- |
| n + 0       | Set title text.                   |
| n + 1       | Set first line of the body text.  |
| n + 2       | Set second line of the body text. |
| ...         | ...                               |
| n + 10      | Set last line of the body text.   |
| n + 1000    | Display test pattern.             |
| n + 1100    | Turn full bright on.              |
| n + 1101    | Turn full bright off.             |
| n + 1200    | Set the color.                    |
| n + 1300    | Clear the entire display.         |
| n + 1301    | Clear the display body text.      |
| n + 1400    | Query the controller's state.     |
| n + 3001    | Select font set 1.                |
| n + 3002    | Select font set 2.                |
| n + 3003    | Select font set 3.                |
| ...         | ...                               |

### Display Size

The size of the board can be altered by adding or removing
display prims and updating their names.
The script assumes a rectangular layout in the way
described in *Object Details*.
The following lines control the shape of the board:

```cpp
// The shape of the board is described here
#define CHARACTER_PER_PRIM 8
#define PRIMS_PER_LINE 5
#define DISPLAY_TITLE_LINES 1
#define DISPLAY_BODY_LINES 10
```

## Object Details

A single ordinary prim is the root object.

The character display is made up of mesh prims
with eight square rectangles to hold eight characters.

The name of each prim indicates its position on the board.
The names are of the format `Ann` where `nn` indicates its
index in a large grid of characters.

The grid of characters is `PRIMS_PER_LINE` prims wide and
`TOTAL_LINES` high. Currently those values are **5** and **11**.
The upper left corner is `A00`. 
Moving left to right:

* The title row is `A00` `A01` `A02`.
  The title only has 24 characters, i.e. three prims.
  The grid is assumed to be five wide; 
  it ignores the missing prims `A03` and `A04`.
* The first body row is `A05` through `A09`,
  40 characters on five prims.
* The second body row is `A10` through `A14`.
* The third body row is `A15` through `A19`.
