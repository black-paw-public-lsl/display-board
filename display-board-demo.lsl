////////////////////////////////////////////////////////////////////////
//
// Demo of the Black Paw Display Board
// by Black Paw Workshop
//
// The master copy of this script is hosted at:
// https://gitlab.com/black-paw-public-lsl/display-board
//
// This work is covered by a Creative Commons
// Attribution-NonCommercial-ShareAlike 4.0 license.
//
// https://creativecommons.org/licenses/by-nc-sa/4.0/
//
////////////////////////////////////////////////////////////////////////

#include "display-board/display-board-demo-constants.lsl"

#define TITLE_TEXT "Black Paw Display Board"
#define WHITE <1,1,1>
#define GOLD <0.8633,0.7227,0.1445>
#define RED <1,0,0>
#define CYAN <0,1,1>
#define GREEN <0,1,0>
#define MAGENTA <1,0,1>

integer displayState;
default
{
    on_rez( integer start_param)
    {
        llResetScript();
    }

    state_entry()
    {
        displayState = -1;
        llSetTimerEvent(0.1);
    }

    link_message( integer sender_num, integer num, string str, key id )
    {
        if(num == DISPLAY_BOARD_STATUS)
        {
            switch(str)
            {
                case DISPLAY_BOARD_STR_READY:
                    DBClearAll();
                    displayState = 0;
                    llSetTimerEvent(2);
                    break;
                case DISPLAY_BOARD_STR_STARTING:
                    displayState = -1;
                    llSetTimerEvent(0.1);
                    break;
            }
        }
    }

    timer()
    {
        switch(displayState)
        {
            case -1:
                DBQueryState();
                // Wait for a while, if nothing is heard from the control script,
                // ask it for its status again
                llSetTimerEvent(8.0);
                break;
            case 0:
                DBFullbrightOn();

                DBFontBoldSerif();
                DBSetColor(WHITE);
                DBDisplayLine(0, TITLE_TEXT);

                DBFontBold();
                DBDisplayLine(1, "Not just a display board.");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 1:
                DBFullbrightOn();
                DBFontBoldSerif();
                DBSetColor(RED);
                DBDisplayLine(0, TITLE_TEXT);

                DBFontBold();
                DBSetColor(WHITE);
                DBDisplayLine(1, "A versatile display board.");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 2:
                DBFullbrightOn();
                DBFontBoldSerif();
                DBSetColor(CYAN);
                DBDisplayLine(0, TITLE_TEXT);

                DBFontBold();
                DBSetColor(WHITE);
                DBDisplayLine(1, "A versatile programmable display board.");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 3:
                DBFullbrightOn();
                DBFontBoldSerif();
                DBSetColor(GOLD);
                DBDisplayLine(0, TITLE_TEXT);

                DBFontBold();
                DBSetColor(WHITE);
                DBDisplayLine(2, "Featuring...");

                DBFullbrightOn();
                DBFontBold();
                DBSetColor(RED);
                DBDisplayLine(3, "Colors.");

                DBSetColor(CYAN);
                DBDisplayLine(4, "All colors.");

                DBSetColor(GOLD);
                DBDisplayLine(5, "Your colors.");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 4:
                DBFullbrightOn();
                DBFontBold();
                DBSetColor(WHITE);
                DBDisplayLine(2, "Featuring... Fonts.");
                DBFontStandard();
                DBDisplayLine(3, "");
                DBDisplayLine(4, "Not just the plain boring fonts seen");
                DBDisplayLine(5, "everywhere...");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 5:
                DBFullbrightOn();
                DBFontBold();
                DBDisplayLine(3, "8 new custom fonts.");
                DBDisplayLine(4, "");
                DBDisplayLine(5, "");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 6:
                DBFontMetal();
                DBSetColor(GOLD);
                DBDisplayLine(4, "For a Medieval look.");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 7:
                DBFontMarker();
                DBSetColor(MAGENTA);
                DBDisplayLine(5, "For a playful look.");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 8:
                DBFontSouthwest();
                DBSetColor(GREEN);
                DBDisplayLine(6, "For a festive look.");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 9:
                DBFontBoldSerif();
                DBSetColor(WHITE);
                DBDisplayLine(7, "For an eye-catching look.");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 10:
                DBFontDripping();
                DBSetColor(RED);
                DBDisplayLine(8, "For a scary look.");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 11:
                DBFontFuture();
                DBSetColor(CYAN);
                DBDisplayLine(9, "For a futuristic look.");

                displayState++;
                llSetTimerEvent(2);
                break;
            case 12:
                DBFontWestern();
                DBSetColor(GREEN);
                DBDisplayLine(10, "And more....");

                displayState++;
                llSetTimerEvent(8);
                break;
            case 13:
                DBFullbrightOn();
                DBFontSouthwest();
                DBSetColor(WHITE);
                DBDisplayLine(1,  "A versatile programmable display board. ");
                DBDisplayLine(2,  "                                        ");
                DBDisplayLine(3,  "         They say size matters.         ");
                DBDisplayLine(4,  "                                        ");
                DBDisplayLine(5,  "                                        ");
                DBDisplayLine(6,  "                                        ");
                DBDisplayLine(7,  "                                        ");
                DBDisplayLine(8,  "                                        ");
                DBDisplayLine(9,  "                                        ");
                DBDisplayLine(10, "                                        ");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 14:
                DBSetColor(WHITE);
                DBDisplayLine(1,  "A versatile programmable display board. ");
                DBDisplayLine(2,  "                                        ");
                DBDisplayLine(3,  "         They say size matters.         ");
                DBDisplayLine(4,  "                                        ");
                DBDisplayLine(5,  "<-------------------------------------->");
                DBDisplayLine(6,  "          40 characters wide            ");
                DBDisplayLine(7,  "                                        ");
                DBDisplayLine(8,  "                                        ");
                DBDisplayLine(9,  "                                        ");
                DBDisplayLine(10, "                                        ");

                displayState++;
                llSetTimerEvent(3.5);
                break;
            case 15:
                DBSetColor(WHITE);
                DBDisplayLine(1,  "A versatile programmable display board.^");
                DBDisplayLine(2,  "                                       |");
                DBDisplayLine(3,  "         They say size matters.        |");
                DBDisplayLine(4,  "                                       |");
                DBDisplayLine(5,  "<-------------------------------------->");
                DBDisplayLine(6,  "          40 characters wide           |");
                DBDisplayLine(7,  "                                       |");
                DBDisplayLine(8,  "                                       |");
                DBDisplayLine(9,  "                         10 lines tall |");
                DBDisplayLine(10, "                          (plus title) V");

                displayState++;
                llSetTimerEvent(5);
                break;
            case 16:
                DBClearText();

                displayState = 0;
                llSetTimerEvent(1);
                break;
        }
    }
}